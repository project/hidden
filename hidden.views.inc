<?php

/**
 * @file hidden.views.inc
 * Provides support for the Views module.
 */

/**
 * Implementation of hook_views_tables().
 */
function hidden_views_tables() {
  $tables['hidden_node'] = array(
    // join table
    'name' => 'hidden_node',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    // fields
    'fields' => array(
      'publicnote' => array(
        'name' => t('Hidden: Node - Public note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the public reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'privatenote' => array(
        'name' => t('Hidden: Node - Private note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the private reason given for hiding content.').' ',
                  t('Use the options field for empty text message if required'),
      ),
      'rid' => array(
        'name' => t('Hidden: Node - Reason'),
        'sortable' => TRUE,
        'handler' => array (
          'hidden_handler_field_reason_title' => t('Title'),
          'hidden_handler_field_reason_description' => t('Description'),
        ),
        'option' => 'string',
        'help' => t('Display the standard reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'created' => array(
        'name' => t('Hidden: Node - Time'),
        'sortable' => TRUE,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display the post time of the node was hidden.').' '.
                  t('The option field may be used to specify the custom date format as it\'s required by the date() function or if "as time ago" has been chosen to customize the granularity of the time interval.'),
      ),
      'hide' => array(
        'name' => t('Node: hide/unhide link'),
        'handler' => 'hidden_handler_hide_link',
        'notafield' => TRUE,
        'addlfields' => array('delay'),
        'help' => t('A hide/unhide link displayed as appropriate'),
      ),
      'report' => array(
        'name' => t('Node: report for hiding link'),
        'handler' => 'hidden_handler_report_link',
        'notafield' => TRUE,
        'addlfields' => array('delay'),
        'help' => t('A report for hiding link displayed as appropriate'),
      ),
    ),
    // filters
    'filters' => array(
      'delay' => array(
        'name' => t("Node: Hidden"),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_hidden_node',
        'option' => hidden_options_filter_hidden_form(),
        'help' => t('Filter on if a node is hidden.'),
      ),
      'rid' => array (
        'name' => t("Hidden: Node - Reason"),
        'value' => hidden_options_filter_reasons_form(),
        'operator' => 'views_handler_operator_or',
        'value-type' => 'array',
        'help' => t('Select by the reason for hiding'),
      ),
    ),
    // sorts
    'sorts' => array(
      'created' => array(
        'name' => t('Hidden: Node - Time'),
        'handler' => 'views_handler_sort_date',
        'option' => views_handler_sort_date_options(),
        'help' => t('Sort by the date of hiding.'),
      ),
    ),
  );

  // base table left is always node (for Views 1.x)
  $tables['hidden_comment'] = array(
    'name' => 'hidden_comment',
    'join' => array(
      'left' => array(
        'table' => 'comments',
        'field' => 'cid',
      ),
      'right' => array (
        'field' => 'cid',
      ),
    ),
    'fields' => array(
      'publicnote' => array(
        'name' => t('Hidden: Comment - Public note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the public reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'privatenote' => array(
        'name' => t('Hidden: Comment - Private note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the private reason given for hiding content.').' ',
                  t('Use the options field for empty text message if required'),
      ),
      'rid' => array(
        'name' => t('Hidden: Comment - Reason'),
        'sortable' => TRUE,
        'handler' => array (
          'hidden_handler_field_reason_title' => t('Title'),
          'hidden_handler_field_reason_description' => t('Description'),
        ),
        'option' => 'string',
        'help' => t('Display the standard reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'created' => array(
        'name' => t('Hidden: Comment - Time'),
        'sortable' => TRUE,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display the post time of the comment was hidden.').' '.
                  t('The option field may be used to specify the custom date format as it\'s required by the date() function or if "as time ago" has been chosen to customize the granularity of the time interval.'),
      ),
      'hide' => array(
        'name' => t('Comment: hide/unhide link'),
        'handler' => 'hidden_handler_hide_link',
        'notafield' => TRUE,
        'addlfields' => array('delay'),
        'help' => t('A hide/unhide link displayed as appropriate'),
      ),
      'report' => array(
        'name' => t('Comment: report for hiding link'),
        'handler' => 'hidden_handler_report_link',
        'notafield' => TRUE,
        'addlfields' => array('delay'),
        'help' => t('A report for hiding link displayed as appropriate'),
      ),
    ),
    'filters' => array (
      'delay' => array(
        'name' => t('Comment: Hidden'),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_hidden_comment',
        'option' => hidden_options_filter_hidden_form(),
        'help' => t('Filter on if a comment is hidden.'),
      ),
      'rid' => array (
        'name' => t('Hidden: Comment - Reason'),
        'value' => hidden_options_filter_reasons_form(),
        'operator' => 'views_handler_operator_or',
        'value-type' => 'array',
        'help' => t('Select by the reason for hiding'),
      ),
    ),
    'sorts' => array(
      'created' => array(
        'name' => t('Hidden: Comment - Time'),
        'handler' => 'views_handler_sort_date',
        'option' => views_handler_sort_date_options(),
        'help' => t('Sort by the date of hiding.'),
      ),
    ),
  );

  // reported nodes
  $tables['hidden_reported_node'] = array(
    'name' => 'hidden_reported_node',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'publicnote' => array(
        'name' => t('Hidden: Reported Node - Public note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the public reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'privatenote' => array(
        'name' => t('Hidden: Reported Node - Private note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the private reason given for hiding content.').' ',
                  t('Use the options field for empty text message if required'),
      ),
      'rid' => array(
        'name' => t('Hidden: Reported Node - Reason'),
        'sortable' => TRUE,
        'handler' => array (
          'hidden_handler_field_reason_title' => t('Title'),
          'hidden_handler_field_reason_description' => t('Description'),
        ),
        'option' => 'string',
        'help' => t('Display the standard reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'created' => array(
        'name' => t('Hidden: Reported Node - Time'),
        'sortable' => TRUE,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display the post time of the node was reported.').' '.
                  t('The option field may be used to specify the custom date format as it\'s required by the date() function or if "as time ago" has been chosen to customize the granularity of the time interval.'),
      ),
    ),
    'filters' => array (
      'repid' => array(
        'name' => t('Node: Reported'),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_reported_node',
        'help' => t('Filter on if the node has been reported for hiding.'),
      ),
      'seen' => array(
        'name' => t('Node: Reported - Seen'),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_reported_node_seen',
        'help' => t('Filter on if the node has been recorded as seen by a moderator.'),
      ),
      'rid' => array (
        'name' => t('Hidden: Reported Node - Reason'),
        'value' => hidden_options_filter_reasons_form(),
        'operator' => 'views_handler_operator_or',
        'value-type' => 'array',
        'help' => t('Select by the reason given for hiding'),
      ),
    ),
    'sorts' => array(
      'created' => array(
        'name' => t('Hidden: Reported node - Time'),
        'handler' => 'views_handler_sort_date',
        'option' => views_handler_sort_date_options(),
        'help' => t('Sort by the date of reporting.'),
      ),
    ),
  );

  // reported comments
  $tables['hidden_reported_comment'] = array(
    'name' => 'hidden_reported_comment',
    'join' => array(
      'left' => array(
        'table' => 'comments',
        'field' => 'cid'
      ),
      'right' => array(
        'field' => 'cid'
      ),
    ),
    'fields' => array(
      'publicnote' => array(
        'name' => t('Hidden: Reported Comment - Public note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the public reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'privatenote' => array(
        'name' => t('Hidden: Reported Comment - Private note'),
        'handler' => 'hidden_handler_field_note',
        'option' => 'string',
        'help' => t('Display the private reason given for hiding content.').' ',
                  t('Use the options field for empty text message if required'),
      ),
      'rid' => array(
        'name' => t('Hidden: Reported Comment - Reason'),
        'sortable' => TRUE,
        'handler' => array (
          'hidden_handler_field_reason_title' => t('Title'),
          'hidden_handler_field_reason_description' => t('Description'),
        ),
        'option' => 'string',
        'help' => t('Display the standard reason given for hiding content.').' '.
                  t('Use the options field for empty text message if required'),
      ),
      'created' => array(
        'name' => t('Hidden: Reported Comment - Time'),
        'sortable' => TRUE,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display the post time of the comment was reported.').' '.
                  t('The option field may be used to specify the custom date format as it\'s required by the date() function or if "as time ago" has been chosen to customize the granularity of the time interval.'),
      ),
    ),
    'filters' => array (
      'repid' => array(
        'name' => t('Comment: Reported'),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_reported_comment',
        'help' => t('Filter on if the comment has been reported for hiding.'),
      ),
      'seen' => array(
        'name' => t('Comment: Reported - Seen'),
        'value' => array(
          '#type' => 'select',
          '#options' => 'views_handler_operator_yesno',
        ),
        'operator' => array('=' => t('Equals')),
        'handler' => 'hidden_handler_filter_reported_comment_seen',
        'help' => t('Filter on if the comment has been recorded as seen by a moderator.'),
      ),
      'rid' => array (
        'name' => t('Hidden: Reported Comment - Reason'),
        'value' => hidden_options_filter_reasons_form(),
        'operator' => 'views_handler_operator_or',
        'value-type' => 'array',
        'help' => t('Select by the reason given for hiding'),
      ),
    ),
    'sorts' => array(
      'created' => array(
        'name' => t('Hidden: Reported comment - Time'),
        'handler' => 'views_handler_sort_date',
        'option' => views_handler_sort_date_options(),
        'help' => t('Sort by the date of reporting.'),
      ),
    ),
  );

  // user table joins
  $tables['hidden_node_users'] = _hidden_views_user_tables('hidden_node', 'uid', t('Hidden: Node - by User'), t('Display the user who hid the node'));
  $tables['hidden_comment_users'] = _hidden_views_user_tables('hidden_comment', 'uid', t('Hidden: comment - by User'), t('Display the user who hid the comment'));
  $tables['hidden_reported_node_byuser'] = _hidden_views_user_tables('hidden_reported_node', 'uid', t('Hidden: Reported Node - by User'), t('Display the user who submitted the report about a node'));
  $tables['hidden_reported_node_seenuser'] = _hidden_views_user_tables('hidden_reported_node', 'suid', t('Hidden: Reported Node - seen by User'), t('Display the user who has seen the reported node'));
  $tables['hidden_reported_comment_byuser'] = _hidden_views_user_tables('hidden_reported_comment', 'uid', t('Hidden: Reported Comment - by User'), t('Display the user who submitted the report about a comment'));
  $tables['hidden_reported_comment_seenuser'] = _hidden_views_user_tables('hidden_reported_comment', 'suid', t('Hidden: Reported Comment - seen by User'), t('Display the user who has seen the reported node'));

  return $tables;
}
function _hidden_views_user_tables($table, $uid, $name, $description) {
  return array(
    'name' => 'users',
    'join' => array( 
      'type' => 'inner',
      'left' => array(
        'table' => $table,
        'field' => $uid,
      ),
      'right' => array(
        'field' => 'uid',
      ),
    ),
    'fields' => array(
      'name' => array(
        'name' => $name,
        'sortable' => TRUE,
        'handler' => 'views_handler_field_username',
        'uid' => 'uid',
        'addlfields' => array('uid'),
        'help' => $description,
      ),
    ),
  );
}

/**
 * Implementation of hook_views_arguments().
 */
function hidden_views_arguments() {
  $arguments = array(
    'node' => array(
      'name' => t("Hidden: Node hidden reason ID"),
      'handler' => "hidden_handler_arg_node",
      'help' => t('Use the reason ID that content was hidden for.'),
    ), 
    'comment' => array(
      'name' => t("Hidden: Comment hidden reason ID"),
      'handler' => "hidden_handler_arg_comment",
      'help' => t('Use the reason ID that content was hidden for.'),
    ),
    'reportednode' => array(
      'name' => t("Hidden: Reported node reason ID"),
      'handler' => "hidden_handler_arg_reportednode",
      'help' => t('Use the reason ID given for hiding content.'),
    ),
    'reportedcomment' => array(
      'name' => t("Hidden: Reported comment reason ID"),
      'handler' => "hidden_handler_arg_reportedcomment",
      'help' => t('Use the reason ID given for hiding content.'),
    ),
  );
  return $arguments;
}

/**
 * Handlers: notafield.
 */
function hidden_handler_hide_link($fieldinfo, $fielddata, $value, $data) {
  if (! user_access('mark as hidden')) { return; }

  if ($data->comments_cid > 0) {
    $delay = 'hidden_comment_delay';
    $type = 'comment';
    $target = $data->comments_cid;
  }
  else {
    $delay = 'hidden_node_delay';
    $type = 'node';
    $target = $data->nid;
  }

  if ($data->$delay == NULL) { // NOT HIDDEN
    return l(t('hide'), "hidden/$type/$target/hide", NULL, drupal_get_destination());
  }
  elseif ($data->$delay == 0) { // HIDDEN
    return l(t('unhide'), "hidden/$type/$target/unhide", NULL, drupal_get_destination());
  }
  else { // DELAYED HIDE
    return l(t('hide now'), "hidden/$type/$target/hide", NULL, drupal_get_destination());
  }
}

function hidden_handler_report_link($fieldinfo, $fielddata, $value, $data) {
  if (! user_access('report for hiding')) { return; }

  if ($data->comments_cid > 0) {
    $delay = 'hidden_comment_delay';
    $type = 'comment';
    $target = $data->comments_cid;
  }
  else {
    $delay = 'hidden_node_delay';
    $type = 'node';
    $target = $data->nid;
  }

  if ($data->delay == NULL || $data->delay > 0) { // NOT YET HIDDEN
    return l(t('report for hiding'), "hidden/$type/$target/report", NULL, drupal_get_destination());
  }
}

/**
 * Options: filters.
 */
function hidden_options_filter_reasons_form() {
  $reasons = hidden_reason_get();

  $options = array( 0 => '<'. t('none') .'>');
  foreach ($reasons as $reason) {
    $options[$reason->rid] = $reason->title;
  }

  return array(
    '#type' => 'select',
    '#options' => $options,
    '#multiple' => TRUE,
  );
}

function hidden_options_filter_hidden_form() {
  return array(
    '#type' => 'select',
    '#default_value' => 1,
    '#options' => array(
      1 => t('Already hidden'),
      2 => t('Already hidden and delayed filter hides'),
      3 => t('Delayed filter hides'),
    ),
  );
}

/**
 * Handlers: fields.
 */

/* publicnote and privatenote */
function hidden_handler_field_note($fieldinfo, $fielddata, $value, $data) {
  $reason = $value;
  if (! $reason) {
    $reason = $fielddata['options'];
  }
 
  return check_markup($reason, FILTER_HTML_STRIP, FALSE);
}

/* rid */
function hidden_handler_field_reason_title($fieldinfo, $fielddata, $value, $data) {
  return _hidden_handler_field_reason('title', $fieldinfo, $fielddata, $value, $data);
}
function hidden_handler_field_reason_description($fieldinfo, $fielddata, $value, $data) {
  return  _hidden_handler_field_reason('description', $fieldinfo, $fielddata, $value, $data);
}
function _hidden_handler_field_reason($field, $fieldinfo, $fielddata, $value, $data) {
  $reason = hidden_reason_get($value);
  if (! $reason->$field) {
    $output = $fielddata['options'];
  }
  else {
    $output = $reason->$field;
  }

  return check_markup($output, FILTER_HTML_STRIP, FALSE);
}

/**
 * Handlers: filters.
 */

/* hidden content */
function hidden_handler_filter_hidden_node($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_hidden('hidden_node', $op, $filter, $filterdata, $query);
}
function hidden_handler_filter_hidden_comment($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_hidden('hidden_comment', $op, $filter, $filterdata, $query);
}
function _hidden_handler_filter_hidden($table, $op, $filter, $filterdata, &$query) {
  switch ($op) {
    case 'handler':
      $query->ensure_table($table);
      switch ($filter['options']) {
        case '1': // Already hidden
          if ($filter['value']) {
            $query->add_where("$table.delay = 0");
          }
          else {
            $query->add_where("ISNULL($table.delay) OR ($table.delay > 0)");
          }
          break;
        case '2': // Already hidden and delayed filter hides
          if ($filter['value']) {
            $query->add_where("$table.delay");
          }
          else {
            $query->add_where("ISNULL($table.delay)");
          }
          break;
        case '3': // Only delayed filter hides
          if ($filter['value']) {
            $query->add_where("$table.delay > 0");
          }
          else {
            $query->add_where("ISNULL($table.delay) OR ($table.delay = 0)");
          }
      }
  }
}

/* reported content */
function hidden_handler_filter_reported_node($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_reported('hidden_reported_node', $op, $filter, $filterdata, $query);
}
function hidden_handler_filter_reported_comment($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_reported('hidden_reported_comment', $op, $filter, $filterdata, $query);
}
function _hidden_handler_filter_reported($table, $op, $filter, $filterdata, &$query) {
  switch ($op) {
    case 'handler':
      $query->ensure_table($table);
      if ($filter['value']) {
        $query->add_where("$table.repid");
      }
      else {
        $query->add_where("ISNULL($table.repid)");
      }
  }
}

/* seen reported content */
function hidden_handler_filter_reported_node_seen($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_reported_seen('hidden_reported_node', $op, $filter, $filterdata, &$query);
}
function hidden_handler_filter_reported_comment_seen($op, $filter, $filterdata, &$query) {
  return _hidden_handler_filter_reported_seen('hidden_reported_comment', $op, $filter, $filterdata, &$query);
}
function _hidden_handler_filter_reported_seen($table, $op, $filter, $filterdata, &$query) {
  switch ($op) {
    case 'handler':
      $query->ensure_table($table);
      if ($filter['value']) { // future proofing presently NULL or 1
        $query->add_where("$table.seen = 1");
      }
      else {
        $query->add_where("ISNULL($table.seen) OR ($table.seen = 0)");
      }
  }
}

/**
 * Handlers: arguments
 */
function hidden_handler_arg_node($op, &$query, $argtype, $arg = '') {
  return _hidden_handler_arg_rid('hidden_node', $op, $query, $argtype, $arg);
}
function hidden_handler_arg_comment($op, &$query, $argtype, $arg = '') {
  return _hidden_handler_arg_rid('hidden_comment', $op, $query, $argtype, $arg);
}
function hidden_handler_arg_reportednode($op, &$query, $argtype, $arg = '') {
  return _hidden_handler_arg_rid('hidden_reported_node', $op, $query, $argtype, $arg);
}
function hidden_handler_arg_reportedcomment($op, &$query, $argtype, $arg = '') {
  return _hidden_handler_arg_rid('hidden_reported_comment', $op, $query, $argtype, $arg);
}
function _hidden_handler_arg_rid($table, $op, &$query, $argtype, $arg) {
  switch($op) {
    case 'summary':
      $query->ensure_table($table);
      $reasonsjoin = array(
        'left' => array(
          'table' => $table,
          'field' => 'rid',
        ),
        'right' => array(
          'field' => 'rid',
        ),
      );
      $query->add_table('hidden_reasons', FALSE, 1, $reasonsjoin);
      $query->add_field('title', 'hidden_reasons');
      $query->add_field('rid', 'hidden_reasons');
      $query->add_field('rid', $table);
      $query->add_where("$table.rid > 0"); // TODO when 0 reason sorted
//      $query->add_groupby("$table.rid");
      $fieldinfo['field'] = "hidden_reasons.title";
      return $fieldinfo;
      break;
    case 'sort':
      $query->add_orderby($table, 'rid', $argtype);
      break;
    case 'filter':
      $rid = (int) $arg;
      $query->ensure_table($table);
      $query->add_field('rid', $table);
      $query->add_where("$table.rid = %d", $rid);
      break;
    case 'link':
      return l($query->title, "$arg/". $query->rid);
    case 'title':
      $reason = hidden_reason_get((int) $arg);
      return check_plain($reason->title);
  }
}
